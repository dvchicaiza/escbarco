/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dchicaiza;
import javax.media.opengl.GL;
/**
 *
 * @author Doris
 */
public class Iceberg {
    GL gl;
    
    public Iceberg(GL gl){
        this.gl=gl;        
    }
    
        void dibujaTriangulo(float x1, float y1, float x2, float y2,float x3, float y3, float c1, float c2, float c3){
        gl.glColor3f(c1,c2,c3);
        gl.glBegin(GL.GL_TRIANGLES);
        gl.glVertex2f(x1, y1);
        gl.glVertex2f(x2, y2);
        gl.glVertex2f(x3, y3);
        gl.glEnd(); 
        }
        
        public void dibujarIceberg(){
            dibujaTriangulo(600, 220, 600, 380, 675, 243, 0.96f, 1, 1);//triangulo derecha
            dibujaTriangulo(530, 245, 600, 360, 600, 220, 1, 1, 1);//medio
            dibujaTriangulo(488, 245, 570, 395, 599, 240, 1, 1, 1);//triangulo izquierda
            
        }   
}
