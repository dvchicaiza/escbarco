/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dchicaiza;
import javax.media.opengl.GL;
/**
 *
 * @author Doris
 */
public class Mar {
   GL gl;
   
   public Mar(GL gl){
        this.gl=gl;
   }
   
   void dibujaCuadrado(float x, float y, float ancho, float alto, float c1, float c2, float c3){   
            gl.glColor3f(c1,c2,c3);
            gl.glBegin(GL.GL_QUADS);
            gl.glVertex2f(x, y);
            gl.glVertex2f(x, y+alto);
            gl.glVertex2f(x+ancho, y+alto);
            gl.glVertex2f(x+ancho, y);
            gl.glEnd();
    }
   
   void dibujaLinea(float x1, float y1, float x2, float y2, float c1, float c2, float c3){
        gl.glColor3f(c1,c2,c3);
        gl.glLineWidth(1.5f);
        gl.glBegin(GL.GL_LINES);
        gl.glVertex2f(x1, y1);
        gl.glVertex2f(x2, y2);
        gl.glEnd();
    }
   public void dibujarMar(){
       dibujaCuadrado(0, 0, 700, 300, 0, 0.78f, 1);  
    
       dibujaLinea(50, 50, 120, 52.5f, 1, 1, 1);
       dibujaLinea(10, 110, 80, 112.5f, 1, 1, 1);
       dibujaLinea(50, 180, 120, 182.5f, 1, 1, 1);
       dibujaLinea(10, 250, 80, 252.5f, 1, 1, 1);
       dibujaLinea(0, 298f, 100, 300, 1, 1, 1);
       dibujaLinea(100, 298f, 200, 300, 1, 1, 1);
       dibujaLinea(200, 298f, 300, 300, 1, 1, 1);
       dibujaLinea(300, 298f, 400, 300, 1, 1, 1);
       dibujaLinea(400, 298f, 500, 300, 1, 1, 1);
       dibujaLinea(500, 298f, 600, 300, 1, 1, 1);
       dibujaLinea(600, 298f, 700, 300, 1, 1, 1);
       
       gl.glTranslatef(180, 0, 0);
       dibujaLinea(50, 50, 120, 52.5f, 1, 1, 1);
       dibujaLinea(230, 50, 300, 52.5f, 1, 1, 1);
       dibujaLinea(390, 50, 480, 52.5f, 1, 1, 1);
       
       dibujaLinea(10, 110, 80, 112.5f, 1, 1, 1);
       dibujaLinea(190, 110, 260, 112.5f, 1, 1, 1);
       dibujaLinea(390, 110, 460, 112.5f, 1, 1, 1);
       
       dibujaLinea(50, 180, 120, 182.5f, 1, 1, 1);
       dibujaLinea(230, 180, 300, 182.5f, 1, 1, 1);
       dibujaLinea(390, 180, 480, 182.5f, 1, 1, 1);
       
       dibujaLinea(10, 250, 80, 252.5f, 1, 1, 1);
       dibujaLinea(190, 250, 260, 252.5f, 1, 1, 1);

   }
   
}
