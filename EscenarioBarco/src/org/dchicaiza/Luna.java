/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dchicaiza;

import javax.media.opengl.GL;
import static java.lang.Math.*;
/**
 *
 * @author Doris
 */
public class Luna {
    GL gl;
    
    public Luna(GL gl){
        this.gl=gl;
    }


    void dibujaCirculo(float posx, float posy, float radio, float c1, float c2, float c3){
        gl.glColor3f(c1, c2, c3);
        gl.glBegin(GL.GL_POLYGON);
        for (float i = 0; i < 360; i++)//va de [0, 360]grados
        {
            float x,y;
            float theta=(float) (i*PI/180);//conversion de grados a radianes
            //conversion de coordenadas polares a rectangulares
            x=(float)(radio*cos(theta));
            y=(float)(radio*sin(theta));
            gl.glVertex2f(x+posx, y+posy);
        }
        gl.glEnd();
    }
    
    public void dibujarLuna(){
        dibujaCirculo(200, 620, 30, 1, 1, 1);
        
    }
    
}

