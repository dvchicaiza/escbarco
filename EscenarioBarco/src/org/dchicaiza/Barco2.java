/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dchicaiza;
import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import javax.media.opengl.GL;
/**
 *
 * @author Doris
 */
public class Barco2 {
    GL gl;
    
    public Barco2(GL gl){
        this.gl=gl;
    }
    
    void dibujaTriangulo(float x1, float y1, float x2, float y2,float x3, float y3, float c1, float c2, float c3){
        gl.glColor3f(c1,c2,c3);
        gl.glBegin(GL.GL_TRIANGLES);
        gl.glVertex2f(x1, y1);
        gl.glVertex2f(x2, y2);
        gl.glVertex2f(x3, y3);
        gl.glEnd(); 
    }
    
    void dibujaCirculo(float posx, float posy, float radio, float c1, float c2, float c3){
        gl.glColor3f(c1, c2, c3);
        gl.glBegin(GL.GL_POLYGON);
        for (float i = 0; i < 360; i++)//va de [0, 360]grados
        {
            float x,y;
            float theta=(float) (i*PI/180);//conversion de grados a radianes
            //conversion de coordenadas polares a rectangulares
            x=(float)(radio*cos(theta));
            y=(float)(radio*sin(theta));
            gl.glVertex2f(x+posx, y+posy);
        }
        gl.glEnd();
    }
    
    void dibujaLinea(float x1, float y1, float x2, float y2, float c1, float c2, float c3){
        gl.glColor3f(c1,c2,c3);
        gl.glLineWidth(3f);
        gl.glBegin(GL.GL_LINES);
        gl.glVertex2f(x1, y1);
        gl.glVertex2f(x2, y2);
        gl.glEnd();
    }
    
    public void dibujarBarco2(){
        //baranda
        dibujaLinea(252, 350, 306, 350, 1, 0, 0);//linea inferior
        dibujaLinea(252, 370, 306, 370, 1, 0, 0);//linea superior
        dibujaLinea(254, 350, 254, 370, 1, 0, 0);//linea izquierda
        dibujaLinea(303, 350, 303, 370, 1, 0, 0);//linea derecha
        dibujaLinea(270, 350, 270, 370, 1, 0, 0);//linea media izquierda
        dibujaLinea(288, 350, 288, 370, 1, 0, 0);//linea media derecha
        dibujaTriangulo(250, 245, 250, 348, 310, 348, 0.6f, 0.4f, 0.2f);//parte delantera barco
        dibujaCirculo(262, 330, 5, 0, 0, 0);
    
    }
}
