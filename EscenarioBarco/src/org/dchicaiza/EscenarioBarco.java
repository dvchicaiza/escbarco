package org.dchicaiza;

import com.sun.opengl.util.Animator;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;



/**
 * EscenarioBarco.java <BR>
 * Programa que dibuja un barco que se desplaza hacia un iceberg que al chocar se parte en dos pedazos y se hunde.
 * author: Doris Chicaiza
 * 
 */
public class EscenarioBarco implements GLEventListener {
    //variables de Opengl
    static GL gl;
    static GLU glu;
    //variables de traslación y rotacion
    static  float tx=0;
    static  float ty=0;
    static  float tx2=0;
    static  float ty2=0;
    float ang1=0;
    
    public static void main(String[] args) {
        Frame frame = new Frame("Escenario Barco");
        GLCanvas canvas = new GLCanvas();

        canvas.addGLEventListener(new EscenarioBarco());
        frame.add(canvas);
        frame.setSize(700, 700);
        final Animator animator = new Animator(canvas);
        frame.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                // Run this on another thread than the AWT event queue to
                // make sure the call to Animator.stop() completes before
                // exiting
                new Thread(new Runnable() {

                    public void run() {
                        animator.stop();
                        System.exit(0);
                    }
                }).start();
            }
        });
        // Center frame
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        animator.start();
    }

    public void init(GLAutoDrawable drawable) {
        // Use debug pipeline
        // drawable.setGL(new DebugGL(drawable.getGL()));

        GL gl = drawable.getGL();
        System.err.println("INIT GL IS: " + gl.getClass().getName());

        // Enable VSync
        gl.setSwapInterval(1);

        // Setup the drawing area and shading mode
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        gl.glShadeModel(GL.GL_SMOOTH); // try setting this to GL_FLAT and see what happens.
    }

    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        GL gl = drawable.getGL();
        GLU glu = new GLU();

        if (height <= 0) { // avoid a divide by zero error!
        
            height = 1;
        }
        final float h = (float) width / (float) height;
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
        //define los limites y produndidad de la ventana
        gl.glOrtho(0, 700, 0, 700, -1.0, 1.0);
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    public void display(GLAutoDrawable drawable) {
        glu = new GLU();
        gl = drawable.getGL();
        // Clear the drawing area
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
        // Reset the current matrix to the "identity"
        gl.glLoadIdentity();
        
        gl.glPushMatrix();
        Mar mar = new Mar(gl);//crea el mar
        mar.dibujarMar();
        gl.glPopMatrix();
        
        
        gl.glPushMatrix();
        Iceberg iceberg = new Iceberg(gl);//crea objeto iceberg
        iceberg.dibujarIceberg();
        gl.glPopMatrix();
        
        
        gl.glPushMatrix();
        //maneja traslacion de la parte trasera del barco mientras cumpla la condicion indicada
        if (tx<238f) {            
            tx=tx+0.2f;
            gl.glTranslatef(tx, 0, 0);//desplaza la parte trasera del barco en el eje X positivo
        } else {
            gl.glTranslatef(236, 0, 0); 
            gl.glRotatef(0.335f,0,1,1); //rota el angulo indicado, la parte trasera del barco
            //traslada la parte trasera del barco en el eje Y negativo
            ty=ty-0.35f;
            gl.glTranslatef(0, ty, 0);
        }
        Barco1 barco1 = new Barco1(gl);//crea la parte trasera del barco
        barco1.dibujarBarco1();
        gl.glPopMatrix();
        
        
        gl.glPushMatrix();
        //maneja traslacion de la parte delantera del barco mientras cumpla la condicion indicada
        if (tx2<236f) {
            tx2=tx2+0.2f;
            gl.glTranslatef(tx2, 0, 0);//desplaza la parte delantera del barco en el eje x positivo
        } else {
            gl.glTranslatef(236, 0, 0);
            gl.glRotatef(0.32f,1,1,0);//rota el angulo indicado, la parte delantera del barco
            //traslada la parte delantera del barco en el eje Y negativo
            ty2=ty2-0.2f;            
            gl.glTranslatef(0, ty2, 0);       
        }
        Barco2 barco2 = new Barco2(gl);//crea la parte delantera del barco
        barco2.dibujarBarco2();
        gl.glPopMatrix();
        
        
        gl.glPushMatrix();
        Luna luna = new Luna(gl);//crea objeto luna
        luna.dibujarLuna();
        gl.glPopMatrix();
        
        
        gl.glPushMatrix();
        gl.glTranslatef(0, -100, 0);
        Mar mar1 = new Mar(gl);//crea el mar1
        mar1.dibujarMar();
        gl.glPopMatrix();
        
        gl.glFlush();
    }

    public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
    }
}

