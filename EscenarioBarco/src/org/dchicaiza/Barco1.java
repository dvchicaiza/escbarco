/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dchicaiza;
import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import javax.media.opengl.GL;
/**
 *
 * @author Doris
 */
public class Barco1 {
    GL gl;
    
    public Barco1(GL gl){
        this.gl=gl;
    }
    
    void dibujaPoligIrregular(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, float c1, float c2, float c3){
            gl.glColor3f(c1, c2, c3);
            gl.glBegin(GL.GL_QUADS);
            gl.glVertex2f(x1, y1);
            gl.glVertex2f(x2, y2);
            gl.glVertex2f(x3, y3);
            gl.glVertex2f(x4, y4);
            gl.glEnd();
    }
    
    void dibujaCuadrado(float x, float y, float ancho, float alto, float c1, float c2, float c3){   
            gl.glColor3f(c1,c2,c3);
            gl.glBegin(GL.GL_QUADS);
            gl.glVertex2f(x, y);
            gl.glVertex2f(x, y+alto);
            gl.glVertex2f(x+ancho, y+alto);
            gl.glVertex2f(x+ancho, y);
            gl.glEnd();
    }
    
    void dibujaLinea(float x1, float y1, float x2, float y2, float c1, float c2, float c3){
        gl.glColor3f(c1,c2,c3);
        gl.glLineWidth(3f);
        gl.glBegin(GL.GL_LINES);
        gl.glVertex2f(x1, y1);
        gl.glVertex2f(x2, y2);
        gl.glEnd();
    }
    
    void dibujaTriangulo(float x1, float y1, float x2, float y2,float x3, float y3, float c1, float c2, float c3){
        gl.glColor3f(c1,c2,c3);
        gl.glBegin(GL.GL_TRIANGLES);
        gl.glVertex2f(x1, y1);
        gl.glVertex2f(x2, y2);
        gl.glVertex2f(x3, y3);
        gl.glEnd(); 
        }
    
    void dibujaCirculo(float posx, float posy, float radio, float c1, float c2, float c3){
        gl.glColor3f(c1, c2, c3);
        gl.glBegin(GL.GL_POLYGON);
        for (float i = 0; i < 360; i++)//va de [0, 360]grados
        {
            float x,y;
            float theta=(float) (i*PI/180);//conversion de grados a radianes
            //conversion de coordenadas polares a rectangulares
            x=(float)(radio*cos(theta));
            y=(float)(radio*sin(theta));
            gl.glVertex2f(x+posx, y+posy);
        }
        gl.glEnd();
    }
    
    public void dibujarBarco1(){
        dibujaPoligIrregular(50, 245, 10, 340, 250, 340, 250, 245, 0.6f, 0.4f, 0.2f);//base
        dibujaCuadrado(82, 300, 30, 20, 0.1f, 0.3f, 0.2f);//ventana izquierda de la base
        dibujaCuadrado(136, 300, 30, 20, 0.1f, 0.3f, 0.2f);//ventana media de la base
        dibujaCuadrado(187, 300, 30, 20, 0.1f, 0.3f, 0.2f);// ventana derecha de la base
        dibujaCuadrado(28, 340, 35, 18, 1, 0, 0);//base bandera
        dibujaLinea(47, 358, 47, 550, 1, 1, 1);//asta bandera
        dibujaTriangulo(8, 525, 46, 540, 46, 505, 1, 0, 0);//bandera
        dibujaCuadrado(80, 340, 140, 45 , 1f, 0.5f, 0f);//seccion media
        //dibuja ventanas de la seccion media
        dibujaCirculo(105, 365, 8, 0.1f, 0.3f, 0.2f);
        dibujaCirculo(135, 365, 8, 0.1f, 0.3f, 0.2f);
        dibujaCirculo(165, 365, 8, 0.1f, 0.3f, 0.2f);
        dibujaCirculo(195, 365, 8, 0.1f, 0.3f, 0.2f);
        
        dibujaCuadrado(90, 385, 60, 70 , 0.5f, 0.0f, 0.0f);//seccion superior 1
        dibujaCuadrado(115, 430, 35, 15, 0.1f, 0.3f, 0.2f);//ventana 1
        dibujaCuadrado(140, 385, 60, 35 , 1.0f, 0.05f, 0.25f);//seccion superior 2
        dibujaCuadrado(165, 400, 35, 15, 0.1f, 0.3f, 0.2f);//ventana 2
    }
}
