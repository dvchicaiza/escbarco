# EscBarco

Programa que usa la librería Opengl en java, para dibujar un barco  frente a un iceberg, el barco se desplaza en el eje x positivo y al chocar con el iceberg, se parte en dos pedazos y se hunde en el mar. 

Los archivos correspondientes al programa se encuentran dentro de la carpeta EscenarioBarco y son los siguientes:

- build
- nbproject
- src/org/dchicaiza
- build.xml
- manifest.mf

La ruta src/org/dchicaiza contiene los archivos .java:

- Barco1.java
- Barco2.java
- Iceberg.java
- Luna.java
- Mar.java
- EscenarioBarco.java(clase principal)

La imagen de ejecución del programa es la siguiente:
![mi ejecucion](https://www.dropbox.com/s/qku18zxze72hi5r/escBarco.PNG?dl=0)
